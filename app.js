require('dotenv').config();

const express = require('express');
const tmi = require('tmi.js');
const fetch = require('node-fetch');

const app = express();

const client = new tmi.client({
    identity: {
        username: process.env.BOT_USERNAME,
        password: process.env.OAUTH_TOKEN
    },
    channels: [
        process.env.CHANNEL_NAME
    ]
});

app.get('*', (req, res) => {
    res.status(403).send('(Forbidden)');
});

client.on('message', onMessage);
client.on('connected', onConnected);

const whitelist = {commands: []};

if (module === require.main) {

    const server = app.listen(process.env.PORT || 9000, async () => {
        const port = server.address().port;

        const result = await ping();
        parsePing(`Server listening on port ${port} check`, result);

        client.connect();

        try {
            const configs = await fetch('https://twitch-api-viewer.firebaseapp.com/raw-commands').then(res => res.json());
        
            const commands = uniqueArray(configs
                .map(x => x.commands)
                .reduce((acc, val) => acc.concat(val), [])
                .filter(x => x)
                .map(x => x.split('(', 1)[0].trim()));
            whitelist.commands = commands;
            console.log(whitelist);
        } catch (error) {
            console.log(error);
        }

    });
}

module.exports = app;

async function onMessage(target, user, message, self) {
    if (self) { return; }

    const msg = message.trim();

    if (msg.indexOf('!') === 0) {

        if(whitelist.commands.length > 0) {
            for (let i = 0; i < whitelist.commands.length; i++) {
                const c_name = whitelist.commands[i];
                if(msg.substring(0, c_name.length) === c_name &&
                    msg.substring(c_name.length, c_name.length + 1).trim() === '') {
                    
                    const result = await ping();
                    parsePing(`Command ${msg} check`, result);

                    if (result.status !== 200) {
                        client.say(target, `@${user['display-name']} main systems are offline atm ask @${process.env.CHANNEL_NAME} about this or please stand by...`);
                    }
                    break;
                }
            }
        } else {
            const result = await ping();
            parsePing(`Possible command ${msg} check`, result);
    
            if (result.status !== 200) {
                client.say(target, `@${user['display-name']} main systems are offline atm ask @${process.env.CHANNEL_NAME} about this or please stand by...`);
            }
        }
    }
}

async function onConnected(addr, port) {
    const result = await ping();
    parsePing(`IRC connected ${addr}:${port} check`, result);
}

function parsePing(message, result) {
    if (!result.error) {
        console.log(`${message} - status: ${result.status} statusText: ${result.statusText}`);
    } else {
        console.log(`${result.error.code}: ${message} - status: ${result.status} statusText: ${result.statusText}`);
    }
}

async function ping() {
    try {
        const { status, statusText } = await fetch(`http://localhost:1338`);

        return { error: null, status, statusText };
    } catch (error) {
        return { error, status: 500, statusText: 'Internal Server Error (ping error)' };
    }
}

// https://stackoverflow.com/questions/9229645/remove-duplicate-values-from-js-array
function uniqueArray(a) {
    var prims = {"boolean":{}, "number":{}, "string":{}}, objs = [];

    return a.filter(function(item) {
        var type = typeof item;
        if(type in prims)
            return prims[type].hasOwnProperty(item) ? false : (prims[type][item] = true);
        else
            return objs.indexOf(item) >= 0 ? false : objs.push(item);
    });
}